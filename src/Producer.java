import java.io.BufferedReader;
import java.io.FileReader;

/**
 * 
 * Generator de evenimente
 * @author Andreea Dobroteanu, 331CA
 *
 */
public class Producer implements Runnable {
	BufferedReader br;
	Buffer buffer;
	int id, readFilesNo;

	Producer(Buffer buffer, int id) {
		this.buffer = buffer;
		this.id = id;
	}
	
	public void readContent(String filename) {
		String line = null;
		try{
			//	Citeste linia
		     br = new BufferedReader(new FileReader(filename));
			while ((line = br.readLine()) != null) {
				String[] data = line.split(",", 3);
				
				//	Asteapta durata specificata
				Thread.sleep(Integer.parseInt(data[0]));
				
				//	Genereaza evenimentul
				Event ev = new Event(Integer.parseInt(data[0]), data[1], Integer.parseInt(data[2]));
				//	Introduce evenimentul generat in coada de evenimente
				buffer.put(ev);
				
			}
			
			br.close();
	  }catch(Exception e){
		  e.printStackTrace();
	  }
	}

	@Override
	public void run() {
		readContent(Main.filenames[id]);
	}
}