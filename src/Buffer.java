import java.util.concurrent.ArrayBlockingQueue;

public class Buffer {	
	public ArrayBlockingQueue<Event> queue = new ArrayBlockingQueue<Event>(Main.queueSize);
	
	int a;

	//	Adauga un nou eveniment in coada de evenimente
	void put(Event value) {
		try {
			queue.put(value);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}

	//	Preia un event din coada
	Event get() throws InterruptedException {
		return queue.take();
	}
	
	//	Verifica daca e coada goala
	boolean isEmpty() {
		return queue.isEmpty();
	}
}
