import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.concurrent.ExecutorService;

/**
 * Worker Pool
 * @author Andreea Dobroteanu, 331CA
 *
 */
class ActionsThread implements Runnable{
	Buffer buffer;
	ExecutorService executor;
	Event event;
	int threadsCounter;
	
	//	Preia urmatorul event din coada de evenimente
	Event getNextEvent() {
		Event value = null;
		while(threadsCounter <= Main.totalEventsNo) {
			try {
				value = buffer.get();
				return value;
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
		return value;
	}
	
	ActionsThread(Buffer buffer, ExecutorService executor, int threadsCounter) {
		this.buffer = buffer;
		this.executor = executor;
		this.threadsCounter = threadsCounter;
		
		this.event = getNextEvent();
	}
	
	@Override
	public void run() {
		if (event == null) {
			try {
				executor.shutdown();
				Main.semaphore.acquire();
				Collections.sort(Main.primeQueue);
				Collections.sort(Main.factQueue);
				Collections.sort(Main.squareQueue);
				Collections.sort(Main.fibQueue);
				
				writeToFile(events.PRIME.toString() + ".out", Main.primeQueue);
				writeToFile(events.FACT.toString() + ".out", Main.factQueue);
				writeToFile(events.SQUARE.toString() + ".out", Main.squareQueue);
				writeToFile(events.FIB.toString() + ".out", Main.fibQueue);
				
				Main.semaphore.release();
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		} else {
			switch(event.eventType) {
				case PRIME:
					prime();
					break;
				case FACT:
					fact();
					break;
				case SQUARE:
					square();
					break;
				case FIB:
					fib();
					break;
				default:
					break;
			}
			
			Main.semaphore.release();
		}
	}
	
	/**
	 * Scrie in fisier
	 * @param fileName - numele fisierului
	 * @param data - lista ce trebuie scrisa in fisier
	 */
	void writeToFile(String fileName, ArrayList<Integer> data) {
		try {
			File file = new File(fileName);

			// Creaza fisierul daca nu exista
			if (!file.exists()) {
				file.createNewFile();
			}

			FileWriter fw = new FileWriter(file.getAbsoluteFile());
			BufferedWriter bw = new BufferedWriter(fw);
			for (int value : data) {
				bw.write(Integer.toString(value) + '\n');
			}
			bw.close();

		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	// Calculeaza cel mai mare numar prim mai mic sau egal N-ul event-ului curent
	void prime() {		
		for (int i = event.N; i > 1; i--) {
			boolean ok = true;
			for (int j = 2; j < i; j++) {
				if (i % j == 0) {
					ok = false;
					break;
				}
			}
			
			if (ok) {
				Main.primeQueue.add(i);
				applyNextAction();
				return;
			}
		}
			
		Main.primeQueue.add(1);
		applyNextAction();
	}

	// Calculeaza cel mai mare numar care are factorialul mai mic sau egal cu N-ul event-ului curent
	void fact() {
		int factorial = 1;
		
	    for (int i = 1; i <= event.N; i++) {
	    	if (factorial * i > event.N) {
	    		Main.factQueue.add(i - 1);
	    		applyNextAction();
	    		return;
	    	}
	    	factorial *= i;
	    }
	}
	
	//	Calculeaza cel mai mare numar care are patratul perfect mai mic sau egal cu N-ul event-ului curent
	void square() {
		for (int i = 0; i < event.N; i++) {
			if (Math.pow(i, 2) > event.N) {
				Main.squareQueue.add(i - 1);
				applyNextAction();
				return;
			}
		}
	}
	
	//	Calculeaza cel mai mare numar pentru care valoarea corespunzatoare din sirul Fibonacci 
	//	este mica sau egala cu N-ul event-ului curent
	void fib() {
		int f1 = 0, f2 = 1, f3;
		
		for (int i = 0; i < event.N; i++) {
			f3 = f1 + f2;
			
			if (f3 > event.N) {
				Main.fibQueue.add(i + 1);
				applyNextAction();
				return;
			}
			
			f1 = f2;
			f2 = f3;
		}
	}
	
	//	Porneste urmatorul thread pentru Worker Pool
	void applyNextAction() {
		executor.submit(new ActionsThread(buffer, executor, ++threadsCounter));
	}
}
