import java.util.ArrayList;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Semaphore;
/**
 * 
 * @author Andreea Dobroteanu, 331CA
 *
 */
public class Main {
	static int queueSize;
	public static int totalEventsNo = 0;
	public static int filesNo;
	
	public static Semaphore semaphore;	
	
	static String filenames[] = new String[20];
	static ArrayList<Event> eventObjects = new ArrayList<Event>();
	
	public static ArrayList<Thread> actionsQueue = new ArrayList<Thread>(); 
	public static ArrayList<Integer> primeQueue = new ArrayList<Integer>();
	public static ArrayList<Integer> factQueue = new ArrayList<Integer>();
	public static ArrayList<Integer> squareQueue = new ArrayList<Integer>();
	public static ArrayList<Integer> fibQueue = new ArrayList<Integer>();
	
	public static void main(String[] args) {
		if (args.length < 3) {
			throw new Error("Please, provide some right arguments: "
					+ "queue_length events_number file_name1 file_name2 [...]");
		}
		
		queueSize = Integer.parseInt(args[0]);
		
		//	Adaug numele fisierelor intr-un vector 
		for (int i = 0; i < args.length - 2; i++) {
			filenames[i] = args[i + 2];
		}
		
		filesNo = args.length - 2;
		
		Buffer buffer = new Buffer();
		
		//	Generatori de evenimente; initializare
		Thread threads[] = new Thread[filesNo];
		for (int i = 0; i < filesNo; i++) {
			threads[i] = new Thread(new Producer(buffer, i));
		}
		
		for (int i = 0; i < filesNo; i++) {
			threads[i].start();
		}
		
		totalEventsNo = filesNo * Integer.parseInt(args[1]) - 1;
		//	Semafor, folosit pentru a detecta cand au terminat toate thread-urile, sa pot sorta valorile
		semaphore = new Semaphore(-totalEventsNo);
		
		// Workers Pool
		Thread actionThread = new Thread();
		ExecutorService executor = Executors.newFixedThreadPool(2);
		actionThread = new Thread(new ActionsThread(buffer, executor, 0));
		actionThread.start();
		
		for (int i = 0; i < filesNo; i++) {
			try {
				threads[i].join();
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
	}
}
