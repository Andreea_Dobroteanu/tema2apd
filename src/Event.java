enum events {PRIME, FACT, SQUARE, FIB}

/**
 * Eveniment; retine datele legate de un eveniment
 * @author Andreea Dobroteanu, 331CA
 *
 */
public class Event {
	int timeInterval;
	events eventType;
	int N;
	
	Event(int timeInterval, String eventName, int N) {
		this.timeInterval = timeInterval;
		this.N = N;
		
		switch(eventName) {
			case "PRIME":
				this.eventType = events.PRIME;
				break;
			case "FACT":
				this.eventType = events.FACT;
				break;
			case "SQUARE":
				this.eventType = events.SQUARE;
				break;
			case "FIB":
				this.eventType = events.FIB;
				break;
			default:
				this.eventType = events.PRIME;
				break;
		}
	}
}
