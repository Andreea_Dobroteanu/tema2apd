
    						Tema 2 APD
    		      Prelucrare paralela de evenimente

    				-------------------------
    				Dobroteanu Andreea, 331CA


-------------------------------------------------------------------------------

Main:
----
* Retin numele fisierelor primite din linia de comanda intr-un vector
* Initializez vectorul de thread-uri pentr generatorii de evenimente cu 
  dimenisunea primita ca argument (numar de fisiere)
* Instantiez thread-urile si le pornesc executia
* Calculez numarul de evenimente totale, existente in toate fisierele
* Instantiez un semafor, caruia ii dau valoarea -numarTotalEvenimente, 
  deoarece vreau sa stiu cand sunt toate executate
* Initializez Workers Pool, cu 2 thread-uri(numarul de core-uri de pe masina
  virtuala pe care am testat).
* Pornesc primul thread din workers pool


Buffer:
------
* Reprezinta coada de evenimente
* Am folosit ArrayBlockingQueue pentru a nu aparea conflicte precum accesarea
  aceluiasi element de catre 2 thread-uri simultan
* Folosit de Producer si ActionsThread


Producer:
--------
* Generatorul de evenimente
* Citeste fisierul, linie cu linie, si aplica operatiile specifice:
  - citeste linia
  - o sparge in 3 elemente(interval de asteptare, operatie, N)
  - asteapta <interval> secunde
  - instantiaza un nou eveniment cu valorile citite
  - il adauga in buffer


Actions:
-------
* Worker Pool
* Foloseste ExecutorService
* La initializare, preia primul event din coada, daca numarul de event-uri
  executate este mai mic sau egal cu cel asteptat; daca nu e niciun event 
  in coada, asteapta sa fie adaugat
* Cand thread-ul este pornit, verifica daca event-ul existent la nivel
  de obiect este null (atunci cand e null, inseamna ca toate evenimentele
  au fost executate)
  - Daca da, sorteaza toate listele cu rezultate si le scrie in fisierele
    spcifice; executia se opreste;
  - Altfel, executa actiunea specifica event-ului curent si incrementeaza
    valoarea semaforului
* In cadrul fiecarei actiuni (prim, fact etc.), atunci cand rezultatul este gasit, 
  este inregistrat in lista specifica, apoi este pornit(submitted) un nou 
  Thread in Worker Pool